﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterStats : MonoBehaviour
{
    public int maxHealth = 100;
    public int currentHealth { get; private set; }

    public HealthBar healthBar;
    public Stat damage;
    public Stat armor;

    private void Awake()
    {
        {
            currentHealth = maxHealth;
            healthBar.SetMaxHealth(maxHealth);
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            TakeDamage(20);
        }
    }
    public void TakeDamage (int damage)
    {

        damage -= armor.GetValue();
        damage = Mathf.Clamp(damage, 3, int.MaxValue);
        currentHealth -= damage;
        healthBar.SetHealth(currentHealth);
        Debug.Log(transform.name + " takes " + damage + " damage.");
        if (currentHealth <= 0)
        {
            Die();
        }
    }

    public virtual void Die ()
    {
        //Die in some way
        //meant to be overwritten
        Debug.Log(transform.name + " died");
    }
}
