﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : CharacterStats
{
    // Start is called before the first frame update
    void Start()
    {
        EquipmentManager.instance.onEquipmentChanged += onEquipmentChanged;
    }

    
    void onEquipmentChanged(Equipment newItem, Equipment olditem)
    {
        if (newItem != null)
        {
            armor.AddModifier(newItem.armourModifier);
            damage.AddModifier(newItem.damageModifier);
        }

        if (olditem != null)
        {
            armor.RemoveModifier(olditem.armourModifier);
            damage.RemoveModifier(olditem.damageModifier);
        }

    }

    public override void Die()
    {
        base.Die();
        PlayerManager.instance.KillPlayer();

    }
}
