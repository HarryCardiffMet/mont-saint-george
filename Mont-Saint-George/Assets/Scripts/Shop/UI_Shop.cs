﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Shop : MonoBehaviour
{
    public Text ClicksTotalText;

    float TotalClicks;

    public Text HealthT;

    float Health = 100;

    public Text CostHealth;

    float Cost = 100;
    public void AddClicks()
    {
        TotalClicks++;
        TotalClicks = TotalClicks + 100;
        ClicksTotalText.text = TotalClicks.ToString("0");
    }

    public void AddHealth()

    {
        if (TotalClicks >= 100)
        {
            Health = Health + 100;
            HealthT.text = Health.ToString("0");
            
            TotalClicks = TotalClicks - 100;
            ClicksTotalText.text = TotalClicks.ToString("0");

            Cost = Cost * 3;
            CostHealth.text = Cost.ToString("0");
        }
        
    }

    public void AddArmour()
    {
        if (TotalClicks >= 150)
        {
            Debug.Log("Armour");
            TotalClicks = TotalClicks - 150;
            ClicksTotalText.text = TotalClicks.ToString("0");
        }
    }

    public void AddSword()
    {
        if (TotalClicks >= 200)
        {
            Debug.Log("Sword");
            TotalClicks = TotalClicks - 200;
            ClicksTotalText.text = TotalClicks.ToString("0");
        }
    }

    public void AddShield()
    {
        if (TotalClicks >= 200)
        {
            Debug.Log("Shield");
            TotalClicks = TotalClicks - 200;
            ClicksTotalText.text = TotalClicks.ToString("0");
        }
    }



}

